const M_PI = 3.14;

function area(iR) {
    return M_PI * iR * iR
}
    
function perimeter(iR) {
    return M_PI * iR * 2
}    
