function area(iA) {
    return iA * iA;
}

function perimeter(iA) {
    return 4 * iA;
}
